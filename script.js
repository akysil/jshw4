function createNewUser() {

    let firstName = prompt('What is your name?');
    let lastName = prompt('What is your last name?');

    function User(name, lastName) {
        this.name = name;
        this.lastName = lastName;
        this.getLogin = function() {
            return `Your login: ${this.name.charAt(0).toLowerCase() + this.lastName.toLowerCase()}`;
        };
    }
    return new User(firstName, lastName);
}

let artem = createNewUser();
console.log(artem.getLogin());

/*
Метод объекта - это функция, которая является свойством объекта, имеющая доступ к данным того же объекта.
 */